<?php
/**
 * Initialize Samair script to fetch proxies from samair.ru website
 * 
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 */
require_once 'src/Samair.php';

$start = microtime(true);
$proxy = new Samair();
$proxy->check();
echo PHP_EOL."Execution time: " . floor((microtime(true) - $start));