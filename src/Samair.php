<?php

/**
 * Class that's used to fetch free proxies from samair.ru website 
 * and store them in database
 * 
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 */
class Samair
{
    private $base_url = 'www.samair.ru/proxy/';
    private $links = [];
    private $proxy_links = [];
    private $proxies = [];
    private $working = [];
    private $pdo;
    
    public function __construct()
    {
        // set these params only for your database
        $user = 'root';
        $pass = '';
        $host = 'localhost';
        $db = 'extraction';
        $this->pdo = new PDO("mysql:host={$host};dbname={$db};charset=utf8", $user, $pass, array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION));

        $this->clearDB();
        $this->getProxyLinks( $this->get_page() );
        $this->getPageLinks();
        $this->getProxyPage();

    }
    
    /**
     * Curl metod to fetch default samair proxy page or specific proxy page
     * 
     * @param  string $link     page link
     * @return string           page content
     */
    private function get_page( $link = null )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $this->base_url . $link );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $page = curl_exec( $ch);
        curl_close( $ch);
        return $page;
    }

    /**
     * Get proxy links from the page
     * 
     * @param  string $page fetched page
     * @return array        array of page links
     */
    private function getProxyLinks( $page )
    {
        //get <a class="page"> tags, and remove duplicate links
        preg_match_all("/<a class=\"page.*?\" href=\"(.*?)\">.*?<\\/a>/", $page, $links);
        $links = array_unique($links[1]);
        foreach($links as $link ) {
            // small hack since we get 'proxy-1.htm' but that page doesn't exist
            if ($link === 'proxy-1.htm') {
                $link = 'proxy-01.htm';
            }
            $this->links[] = $link;
        }
    }

    /**
     * Get page links that we will use to get proxy list
     * 
     * @return array    proxy page links
     */
    private function getPageLinks()
    {
        foreach ($this->links as $link) {
            // fetch page
            $page = $this->get_page( $link );
            // match tag to pull link that we need to visit for proxies
            preg_match_all("/<a href=\"\\/proxy\\/ip-port(.*?)\">.*?<\\/a>/", $page, $proxy_link);
            // store proxy in the list

            $this->proxy_links[] = 'ip-port' . $proxy_link[1][0];
        }

    }

    /**
     * Get proxy page and pull proxies
     * 
     * @return array    proxy list
     */
    private function getProxyPage()
    {
        foreach ($this->proxy_links as $proxy_link) {
            // get proxy link page
            $page = $this->get_page( $proxy_link );
            // get content of <pre></pre> tag
            preg_match_all("/<pre>(.*?)<\\/pre>/s", $page, $list);
            
            // process matched  data and store it in array
            foreach(explode("\n", $list[1][0]) as $proxy) {
                if (!empty($proxy)) {
                    array_push($this->proxies, $proxy);
                }
            }
        }
    }

    /**
     * Check fetched proxies, test them and store only anonymous and elite in DB
     * 
     * @return void 
     */
    public function check()
    {
        $mc = curl_multi_init ();
        for ($thread_no = 0; $thread_no<count ($this->proxies); $thread_no++)
        {
            // set multi curl options
            $c [$thread_no] = curl_init ();
            curl_setopt ($c [$thread_no], CURLOPT_URL, "http://176.221.75.143/ping.php");
            curl_setopt ($c [$thread_no], CURLOPT_HEADER, 1);
            curl_setopt ($c [$thread_no], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($c [$thread_no], CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt ($c [$thread_no], CURLOPT_TIMEOUT, 10);
            curl_setopt ($c [$thread_no], CURLOPT_PROXY, trim ($this->proxies [$thread_no]));
            curl_setopt ($c [$thread_no], CURLOPT_PROXYTYPE, 0);
            curl_setopt ($c [$thread_no], CURLOPT_POST, true);
            curl_setopt ($c [$thread_no], CURLOPT_POSTFIELDS, array( 'r' => 'request') );
            curl_setopt ($c [$thread_no], CURLOPT_COOKIE, 'c=cookie');
            curl_setopt ($c [$thread_no], CURLOPT_REFERER, 'http://www.google.com');
            curl_setopt ($c [$thread_no], CURLOPT_USERAGENT, 'Mozila/4.0');
            curl_multi_add_handle ($mc, $c [$thread_no]);
        }

        do {
            while (($execrun = curl_multi_exec ($mc, $running)) == CURLM_CALL_MULTI_PERFORM);
            if ($execrun != CURLM_OK) break;
            while ($done = curl_multi_info_read ($mc))
            {
                $info = curl_getinfo ($done ['handle']);
                // we accept only those that gets http response code 200 (success)
                if ($info['http_code'] == 200) {
                    try {
                        $content = curl_multi_getcontent($done ['handle']);
                        $proxy = trim ($this->proxies [array_search ($done['handle'], $c)]);
                        $res = $this->checkProxyContent($content, $info);
                        // check proxy level and store only elite/anonymous proxies, discard others
                        switch($res['proxy_level']) {
                            case "transparent":
                                //echo "Bad proxy: {$proxy}".PHP_EOL;
                            break;
                            case "anonymous":
                                echo "Anonymous proxy: {$proxy}".PHP_EOL;
                                array_push($this->working, $proxy);
                                $this->saveProxy($proxy);
                            break;
                            case "elite":
                                echo "ELITE proxy: {$proxy}".PHP_EOL;
                                array_push($this->working, $proxy);
                                $this->saveProxy($proxy);
                            break;
                        }
                    } catch (Exception $e) {
                        print_r($e->getMessage() . PHP_EOL);
                    }
                }
                curl_multi_remove_handle ($mc, $done ['handle']);
            }
        } while ($running);
        curl_multi_close ($mc);

    }

    /**
     * Save passed proxy in the database
     * @param  string $proxy  proxy:port
     * @return void        
     */
    protected function saveProxy($proxy)
    {
        $proxy = explode(':', $proxy);
        $ip = $proxy[0];
        $port = $proxy[1];
        $query = $this->pdo->prepare("INSERT INTO `free_proxies` (ip_address, port, is_anonymous, country_code) values('{$ip}', '{$port}', 1, 'US')");
        $query->execute();
    }

    /**
     * Clear database on initialization to store fresh proxies
     * 
     * @return void 
     */
    protected function clearDB()
    {
        $this->pdo->query("DELETE FROM `free_proxies`");
        $this->pdo->query("TRUNCATE TABLE `free_proxies`");
    }

    /**
     * Check proxy state
     * 
     * @param  string $content fetched content/headers
     * @param  array $info     curl info response array
     * @return array           proxy test state info
     */
    private function checkProxyContent($content, $info)
    {
        $check = array('get', 'post', 'cookie', 'referer', 'user_agent');

        if (!$content) {
            throw new \Exception('Empty content');
        }

        if (!strpos($content, 'check this string in proxy response content')) {
            throw new \Exception('Wrong content');
        }

        $allowed = array();
        $disallowed = array();

        foreach ($check as $value) {
            if (strpos($content, "allow_$value")) {
                $allowed[] = $value;
            } else {
                $disallowed[] = $value;
            }
        }

        // proxy level
        $proxyLevel = '';
        if (strpos($content, 'proxylevel_elite')) {
            $proxyLevel = 'elite';
        } elseif (strpos($content, 'proxylevel_anonymous')) {
            $proxyLevel = 'anonymous';
        } elseif (strpos($content, 'proxylevel_transparent')) {
            $proxyLevel = 'transparent';
        }

        return array(
            'allowed'     => $allowed,
            'disallowed'  => $disallowed,
            'proxy_level' => $proxyLevel,
            'info'        => $info
        );
    }
}